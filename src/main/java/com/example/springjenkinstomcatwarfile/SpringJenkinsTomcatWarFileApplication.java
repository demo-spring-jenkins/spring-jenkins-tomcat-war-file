package com.example.springjenkinstomcatwarfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJenkinsTomcatWarFileApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJenkinsTomcatWarFileApplication.class, args);
    }

}
