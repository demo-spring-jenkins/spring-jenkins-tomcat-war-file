package com.example.springjenkinstomcatwarfile.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TomcatController {
    @GetMapping("/hello")
    public String sayHelloJenkins() {
        return "Hello jenkins tomcat war file";
    }
}
